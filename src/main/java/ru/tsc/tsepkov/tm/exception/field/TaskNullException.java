package ru.tsc.tsepkov.tm.exception.field;

public final class TaskNullException extends AbstractFieldException {

    public TaskNullException() {
        super("Error! Task is null...");
    }

}
