package ru.tsc.tsepkov.tm.service;

import ru.tsc.tsepkov.tm.api.repository.IProjectRepository;
import ru.tsc.tsepkov.tm.api.service.IProjectTaskService;
import ru.tsc.tsepkov.tm.api.repository.ITaskRepository;
import ru.tsc.tsepkov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.tsepkov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.tsepkov.tm.exception.field.ProjectIdEmptyException;
import ru.tsc.tsepkov.tm.exception.field.TaskIdEmptyException;
import ru.tsc.tsepkov.tm.exception.field.UserIdEmptyException;
import ru.tsc.tsepkov.tm.model.Task;
import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(
            final IProjectRepository projectRepository,
            final ITaskRepository taskRepository
    ) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskToProject(final String userId, final String projectId, final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
    }

    @Override
    public void unbindTaskFromProject(final String userId, final String projectId, final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
    }

    @Override
    public void removeProjectById(final String userId, final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        final List<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
        for (final Task task : tasks) taskRepository.removeOneById(userId, task.getId());
        projectRepository.removeOneById(projectId);
    }

}
