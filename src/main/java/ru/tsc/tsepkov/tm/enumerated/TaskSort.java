package ru.tsc.tsepkov.tm.enumerated;

import ru.tsc.tsepkov.tm.comparator.CreatedComparator;
import ru.tsc.tsepkov.tm.comparator.NameComparator;
import ru.tsc.tsepkov.tm.comparator.StatusComparator;
import ru.tsc.tsepkov.tm.model.Task;

import java.util.Comparator;

public enum TaskSort {

    BY_NAME("Sort by name", NameComparator.INSTANCE::compare),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE::compare),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE::compare),
    BY_DEFAULT("Sort by default", null);

    private final String name;

    private final Comparator<Task> comparator;

    public static TaskSort toSort(final String value) {
        if (value == null || value.isEmpty()) return BY_DEFAULT;
        for (final TaskSort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return BY_DEFAULT;
    }

    TaskSort(final String name, Comparator<Task> comparator) {
        this.name = name;
        this.comparator = comparator;
    }

    public String getName() {
        return name;
    }

    public Comparator<Task> getComparator() {
        return comparator;
    }

}
