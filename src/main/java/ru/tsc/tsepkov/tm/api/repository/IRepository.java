package ru.tsc.tsepkov.tm.api.repository;

import ru.tsc.tsepkov.tm.model.AbstractModel;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    M add(M model);

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    M findOneById(String id);

    M findOneByIndex(Integer index);

    M removeOne(M model);

    M removeOneById(String id);

    M removeOneByIndex(Integer index);

    boolean existsById(String id);

    void removeAll();

    int getSize();

}
