package ru.tsc.tsepkov.tm.command.system;

import ru.tsc.tsepkov.tm.api.model.ICommand;
import ru.tsc.tsepkov.tm.command.AbstractCommand;
import java.util.Collection;

public final class CommandListCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = "-cmd";

    public static final String NAME = "commands";

    public static final String DESCRIPTION = "Show command list.";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command: commands) {
            if (command == null) continue;
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name.toString());
        }
    }

}
