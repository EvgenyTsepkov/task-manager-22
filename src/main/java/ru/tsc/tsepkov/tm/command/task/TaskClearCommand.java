package ru.tsc.tsepkov.tm.command.task;

public final class TaskClearCommand extends AbstractTaskCommand {

    public static final String NAME = "task-clear";

    public static final String DESCRIPTION = "Remove all tasks.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[TASKS CLEAR]");
        final String userId = getUserId();
        getTaskService().removeAll(userId);
    }

}
