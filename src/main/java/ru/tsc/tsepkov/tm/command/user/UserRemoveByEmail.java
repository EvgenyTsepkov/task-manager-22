package ru.tsc.tsepkov.tm.command.user;

import ru.tsc.tsepkov.tm.enumerated.Role;
import ru.tsc.tsepkov.tm.util.TerminalUtil;

public final class UserRemoveByEmail extends AbstractUserCommand {

    public static final String NAME = "remove-user-by-email";

    public static final String DESCRIPTION = "Remove user by email.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER BY EMAIL]");
        System.out.println("ENTER EMAIL:");
        final String email = TerminalUtil.nextLine();
        serviceLocator.getUserService().removeByEmail(email);
    }

    @Override
    public Role[] getRoles() {
        return new Role[] {Role.ADMIN};
    }

}
