package ru.tsc.tsepkov.tm.command.project;

import ru.tsc.tsepkov.tm.enumerated.ProjectSort;
import ru.tsc.tsepkov.tm.model.Project;
import ru.tsc.tsepkov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    public static final String NAME = "project-list";

    public static final String DESCRIPTION = "Show list projects.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(ProjectSort.values()));
        final String sortType = TerminalUtil.nextLine();
        final ProjectSort sort = ProjectSort.toSort(sortType);
        final String userId = getUserId();
        final List<Project> projects = getProjectService().findAll(userId, sort.getComparator());
        int index = 1;
        for (final Project project: projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project.getName() + " : " + project.getDescription());
            index++;
        }
    }

}

