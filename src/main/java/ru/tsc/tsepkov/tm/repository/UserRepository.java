package ru.tsc.tsepkov.tm.repository;

import ru.tsc.tsepkov.tm.api.repository.IUserRepository;
import ru.tsc.tsepkov.tm.model.User;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByLogin(final String login) {
        return records.stream()
                .filter(m -> login.equals(m.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User findByEmail(final String email) {
        return records.stream()
                .filter(m -> email.equals(m.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public Boolean isLoginExist(final String login) {
        return records.stream()
                .anyMatch(m -> login.equals(m.getLogin()));
    }

    @Override
    public Boolean isEmailExist(final String email) {
        return records.stream()
                .anyMatch(m -> email.equals(m.getEmail()));
    }

}
