package ru.tsc.tsepkov.tm.repository;

import ru.tsc.tsepkov.tm.api.repository.ITaskRepository;
import ru.tsc.tsepkov.tm.model.Task;
import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        return records.stream()
                .filter(m -> m.getProjectId() != null)
                .filter(m -> userId.equals(m.getUserId()))
                .filter(m -> projectId.equals(m.getProjectId()))
                .collect(Collectors.toList());
    }

}
